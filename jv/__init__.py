import sys
# from PySide2.QtCore import *
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QApplication, QDialog, QMenu, QSystemTrayIcon
import pystray
from PIL import Image, ImageDraw

from jv._version import __version__, __version_info__     # noqa: F401


class QtTrayApp:
    def __init__(self):
        # Create a Qt application
        self.app = QApplication(sys.argv)

        icon = QIcon("audio-volume-high.png")
        menu = QMenu()
        settingAction = menu.addAction("setting")
        settingAction.triggered.connect(self.setting)
        exitAction = menu.addAction("exit")
        exitAction.triggered.connect(sys.exit)

        self.tray = QSystemTrayIcon()
        self.tray.setIcon(icon)
        self.tray.setContextMenu(menu)
        self.tray.show()
        self.tray.setToolTip("unko!")
        # self.tray.showMessage("hoge", "moge")
        # self.tray.showMessage("fuga", "moge")

    def run(self):
        # Enter Qt application main loop
        self.app.exec_()
        sys.exit()

    def setting(self):
        self.dialog = QDialog()
        self.dialog.setWindowTitle("Setting Dialog")
        self.dialog.show()


class Tray(pystray.Icon):
    def __init__(self, *args, **kwargs):
        super(Tray, self).__init__('my name', *args, **kwargs)
        self.icon = self.create_image()
        self.menu = pystray.Menu(
            pystray.MenuItem('my text', action=self.close),
        )
        self.visible = True
        self.run(input)

    @staticmethod
    def create_image():
        width, height = 16, 16
        color1 = '#333333'
        color2 = '#AAAAAA'

        # Generate an image and draw a pattern
        image = Image.new('RGB', (width, height), color1)
        dc = ImageDraw.Draw(image)
        dc.rectangle(
            (width // 2, 0, width, height // 2),
            fill=color2)
        dc.rectangle(
            (0, height // 2, width // 2, height),
            fill=color2)

        return image

    def close(self):
        self.visible = False
        self.stop()


def main():
    # app = QtTrayApp()
    # app.run()
    tray = Tray()


if __name__ == '__main__':
    main(*sys.argv[1:])
